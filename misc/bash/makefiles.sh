#!/usr/bin/bash
################################################################################
#                              scriptTemplate                                  #
#                                                                              #
# Use this template as the beginning of a new program. Place a short           #
# description of the script here.                                              #
#                                                                              #
# Change History                                                               #
# 02/20/2023  Shafan Sugarman Augmented code. Used to make files.              #
# 11/11/2019  David Both    Original code. This is a template for creating     #
#                           new Bash shell scripts.                            #
#                           Add new history entries as needed.                 #
#                                                                              #
#                                                                              #
################################################################################
################################################################################
################################################################################
#                                                                              #
#  Copyright (C) 2007, 2019 David Both, Shafan Sugarman                        #
#  LinuxGeek46@both.org, dev.sugarman@gmail.com                                #
#                                                                              #
#  This program is free software; you can redistribute it and/or modify        #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation; either version 2 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  This program is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with this program; if not, write to the Free Software                 #
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   #
#                                                                              #
################################################################################
################################################################################
################################################################################

################################################################################
# Help                                                                         #
################################################################################
Help()
{
   # Display Help
#   echo "Add description of the script functions here."
   echo -e "On default this program will make 25 files.\nIf the files exsist, 25 files will be added from the highest current value.\nIf you wish to make new files for scripting purpose.\nInput the parameters asked below.\nYou will be prompted if you wish to make the files executible."
   echo
#   echo "Syntax: scriptTemplate [-g|h|t|v|V]"
   echo "Syntax: makefiles.sh newFileName newFileExtension amountOfFiles programmingLangugeUsed [-h]"
   echo "options:"
#   echo "g     Print the GPL license notification."
   echo "h     Print this help."
#   echo "v     Verbose mode."
#   echo "V     Print software version and exit."
   echo
}

################################################################################
################################################################################
# Main program                                                                 #
################################################################################
################################################################################
################################################################################
# Process the input options. Add options as needed.                            #
################################################################################
# Get the options
while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
     \?) # incorrect option
         echo "Error: Invalid option"
         exit;;
   esac
done

proglng=$4

# This function uses a single space as a delimiter along with the whereis command to find the path of the binaries.
function cutfunc ()
{
   local LBINPATH=$(whereis $proglng | echo -e '#!'`cut -d " " -f 2`);
   echo $LBINPATH
}

BINPATH="$(cutfunc)"
echo $BINPATH

echo "makes 25 files."
