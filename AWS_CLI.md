# AWS CLI commands and notes

### Get AWS region of host
 - `curl http://169.254.169.254/latest/dynamic/instance-identity/document | grep region`
 ### List sizes and content of bucket
 - `aws s3api list-objects --bucket BUCKETNAME --output json --query "[sum(Contents[].Size), length(Contents[])]"`