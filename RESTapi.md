# REST API
Stands for Representational State Transfer
- Resource Based (not action based like SOAP)
- Uniform Interface, Stateless, Client-Server, Cacheable, Layered System, Code on demand
- HTTP Requests are made via endpoints
- User receives responses that include certain information like status code: Success - 2xx, Redirect - 3xx, Client-Side Error 4xx, Server-side Error 5xx
- Different types of Authentication: 'No Auth' (Public), 'Auth' (user/password), API Key, etc.

![HTTP Request Components](/images/requestcomp.png)

*Request Components*

![URI Diagram](/images/uridiagram.png)

*A URI Has many parts*

## Helpful Videos

[![Uploading Files to S3 in Python Using Boto3](/images/boto3.png)](https://youtu.be/7gqvV4tUxmY)
<br><br>
<https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html>
<br><br>
[Boto3 basics](https://predictivehacks.com/a-basic-introduction-to-boto3/)
